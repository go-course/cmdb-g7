package all

import (
	// 注册所有HTTP服务模块, 暴露给框架HTTP服务器加载
	_ "gitee.com/go-course/cmdb-g7/apps/book/api"
	_ "gitee.com/go-course/cmdb-g7/apps/host/api"
	_ "gitee.com/go-course/cmdb-g7/apps/resource/api"
	_ "gitee.com/go-course/cmdb-g7/apps/secret/api"
	_ "gitee.com/go-course/cmdb-g7/apps/task/api"
)
