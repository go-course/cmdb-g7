package all

import (
	// 注册所有GRPC服务模块, 暴露给框架GRPC服务器加载, 注意 导入有先后顺序
	_ "gitee.com/go-course/cmdb-g7/apps/book/impl"
	_ "gitee.com/go-course/cmdb-g7/apps/host/impl"
	_ "gitee.com/go-course/cmdb-g7/apps/resource/impl"
	_ "gitee.com/go-course/cmdb-g7/apps/secret/impl"
	_ "gitee.com/go-course/cmdb-g7/apps/task/impl"
)
